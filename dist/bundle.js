/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = React;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(0);
var ReactDOM = __webpack_require__(2);
var ShopHome_1 = __webpack_require__(3);
ReactDOM.render(React.createElement(ShopHome_1.ShopHome, { shopName: "Clothing Bag Shop" }), document.getElementById("example"));


/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = ReactDOM;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(0);
var product_1 = __webpack_require__(4);
var ShopHome = /** @class */ (function (_super) {
    __extends(ShopHome, _super);
    function ShopHome(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            products: []
        };
        return _this;
    }
    ShopHome.prototype.componentDidMount = function () {
        var _this = this;
        fetch('http://localhost:3000/data/products.json')
            .then(function (response) { return response.json(); })
            .then(function (data) {
            console.log('Response: ', data);
            _this.setState({ products: data });
        });
    };
    ShopHome.prototype.render = function () {
        var styles = {
            margin: 10,
            padding: 5
        };
        var inputStyle = {
            width: 30,
            margin: 5
        };
        var items = this.state.products.map(function (item, id) {
            return (React.createElement("div", { key: id },
                React.createElement(product_1.ProductComponent, { productname: item.ProductName, price: item.ProductPrice, discount: item.DiscountPercent, gender: item.Gender, imageUrl: item.ImageName, quantity: item.quantity })));
        });
        return (React.createElement("div", null,
            React.createElement("h1", null, this.props.shopName),
            React.createElement("div", { className: "panel-group" }, items)));
    };
    return ShopHome;
}(React.Component));
exports.ShopHome = ShopHome;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(0);
var ProductComponent = /** @class */ (function (_super) {
    __extends(ProductComponent, _super);
    function ProductComponent(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            productname: _this.props.productname,
            price: _this.props.price,
            discount: _this.props.discount,
            gender: _this.props.gender,
            imageUrl: _this.props.imageUrl,
            quantity: _this.props.quantity
        };
        _this.onAddToCart = _this.onAddToCart.bind(_this);
        _this.onQuantityChange = _this.onQuantityChange.bind(_this);
        return _this;
    }
    ProductComponent.prototype.onQuantityChange = function (event) {
        console.log('Value of event quantity: ', event.target.value);
        this.setState({ quantity: event.target.value });
    };
    ProductComponent.prototype.onAddToCart = function (event) {
        alert('A quantity was submitted: ' + this.state.quantity);
        event.preventDefault();
    };
    ProductComponent.prototype.render = function () {
        var styles = {
            margin: 10,
            padding: 5
        };
        var inputStyle = {
            width: 30,
            margin: 5
        };
        var imgstyles = {
            margin: 2,
            padding: 5
        };
        return (React.createElement("form", { onSubmit: this.onAddToCart },
            React.createElement("div", { className: "col-md-3 col-sm-6 col-xs-6 panel panel-primary", style: styles },
                React.createElement("div", { className: "panel-heading" },
                    React.createElement("strong", null, this.state.productname)),
                React.createElement("div", { className: "panel-body" },
                    React.createElement("div", { className: "row" },
                        React.createElement("div", { className: "col-md-6 col-lg-6 col-sm-6 col-xs-6" },
                            React.createElement("div", null,
                                "Gender: ",
                                this.state.gender),
                            React.createElement("div", null,
                                "Price: $ ",
                                this.state.price),
                            React.createElement("div", null,
                                "Discount %: ",
                                this.state.discount)),
                        React.createElement("div", { className: "col-md-6 col-lg-6 col-sm-6 col-xs-6" },
                            React.createElement("div", { style: imgstyles },
                                React.createElement("img", { className: "image img img-responsive img-thumbnail", src: this.state.imageUrl, title: this.state.imageUrl })))),
                    React.createElement("div", { className: "panel-footer" },
                        React.createElement("span", null,
                            " Quantity:",
                            React.createElement("input", { type: "number", name: "quantity", style: inputStyle, value: this.state.quantity, onChange: this.onQuantityChange })),
                        React.createElement("input", { type: "submit", value: "Add to cart", name: "btnCheckout" }))))));
    };
    return ProductComponent;
}(React.Component));
exports.ProductComponent = ProductComponent;


/***/ })
/******/ ]);
//# sourceMappingURL=bundle.js.map