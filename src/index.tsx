import * as React from "react";
import * as ReactDOM from "react-dom";

import { ShopHome } from "./containers/ShopHome";

ReactDOM.render(
    <ShopHome shopName="Clothing Bag Shop" />,
    document.getElementById("example")
);