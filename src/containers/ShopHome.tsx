import * as React from "react";
import { ProductComponent } from '../components/product/product';

export interface ShopProps { shopName: string; }

export class ShopHome extends React.Component<ShopProps, undefined> {
    state: any;
    constructor(props: any) {
        super(props);
        this.state = {
            products: []
        };
    }
    componentDidMount() {
        fetch('http://localhost:3000/data/products.json')
            .then(response => response.json())
            .then((data) => {
                console.log('Response: ', data);
                this.setState({ products: data });
            });
    }
    render() {
        const styles = {
            margin: 10,
            padding: 5
        };
        const inputStyle = {
            width: 30,
            margin: 5
        }
        const items = this.state.products.map((item: any, id: any) => {
            return (
                <div key={id}>
                    <ProductComponent
                        productname={item.ProductName}
                        price={item.ProductPrice}
                        discount={item.DiscountPercent}
                        gender={item.Gender}
                        imageUrl={item.ImageName}
                        quantity={item.quantity}
                    />
                </div>

            )
        })
        return (<div>

            <h1>{this.props.shopName}</h1>
            <div className="panel-group">
                {items}
            </div>
        </div>
        );
    }
}