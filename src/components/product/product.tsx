import * as React from "react";

//export interface HelloProps { compiler: string; framework: string; }
export interface ProductProps {
    productname: string;
    price: number;
    gender: string;
    discount: number;
    imageUrl: string;
    quantity: number;
}

export class ProductComponent extends React.Component<ProductProps, undefined> {
    state: any;
    constructor(props: any) {
        super(props);
        this.state = {
            productname: this.props.productname,
            price: this.props.price,
            discount: this.props.discount,
            gender: this.props.gender,
            imageUrl: this.props.imageUrl,
            quantity: this.props.quantity
        }
        this.onAddToCart = this.onAddToCart.bind(this);
        this.onQuantityChange = this.onQuantityChange.bind(this);
    }

    onQuantityChange(event: any) {
        console.log('Value of event quantity: ', event.target.value);
        this.setState({ quantity: event.target.value });
    }

    onAddToCart(event: any) {
        alert('A quantity was submitted: ' + this.state.quantity);
        event.preventDefault();
    }
    render() {
        const styles = {
            margin: 10,
            padding: 5
        };
        const inputStyle = {
            width: 30,
            margin: 5
        }
        const imgstyles = {
            margin: 2,
            padding: 5
        };

        return (
            <form onSubmit={this.onAddToCart}>
                <div className="col-md-3 col-sm-6 col-xs-6 panel panel-primary" style={styles}>

                    <div className="panel-heading"><strong>{this.state.productname}</strong></div>

                    <div className="panel-body">
                        <div className="row">
                            <div className="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                <div>Gender: {this.state.gender}</div>
                                <div>Price: $ {this.state.price}</div>
                                <div>Discount %: {this.state.discount}</div>
                            </div>
                            <div className="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                <div style={imgstyles} >
                                    <img 
                                        className="image img img-responsive img-thumbnail" 
                                        src={this.state.imageUrl} 
                                        title={this.state.imageUrl} />
                                </div>
                            </div>
                        </div>
                        <div className="panel-footer">

                            <span> Quantity:
                            <input
                                    type="number"
                                    name="quantity"
                                    style={inputStyle}
                                    value={this.state.quantity}
                                    onChange={this.onQuantityChange} /></span>
                            <input type="submit" value="Add to cart" name="btnCheckout" />
                        </div>
                       
                    </div>
                </div>
            </form>
                );
    }
}